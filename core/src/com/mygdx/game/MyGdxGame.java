package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

public class MyGdxGame extends ApplicationAdapter {





    //
    private int screenWidth, screenHeight;
    private  OrthographicCamera oc;
    private  SpriteBatch batch;
    private  Texture img;
    private  Texture walkList, jumpList, idleList, punchList, box, box2, boxw;
    private   TextureRegion tr;
    private   BitmapFont bf;
    private  Sprite sprite;
    private  int frame = 0;
    private  TextureAtlas ta;
    private  boolean direction = false;
    private  Animation walkAnimation, jumpAnimation, idleAnimation, punchAnimation;
    private float time = 0;
    private boolean rightDirection = true;
    private int width = 68;
    private  int height = 98;
    private  Player p;
    private   List<WoodBox> woodBoxes;
    private String map[] = new String[]{
        "bxxxxxxxbbbxxxxxxxxxbbbbbbbbbbbbbbbxxbb0", //0
        "xx                           x       xx0", //1
        "xx                         b        x x0", //2
        "xxxxx    x                          xxx0",
        "xxxxx    x                          xxx0",
        "xxxxx    x                          xxx0",
        "xxxxx    x                           xx0",
        "xxxxx    x       x            xxx    xx0",//3
        "xxxx           x                    xxx0", //4
        "xx           xx                      xx0", //5
        "x   xx     xxxbbxxx           xxx    xx0", //6
        "xx       xxxxxxxxxxxxxx             xxx0", //7
        "xx      xxxxxxbbbbbbbbxxxxx        xxxx0", //8
        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   xxxxx0",
        "xxxxx     x                         xxx0",
        "xxxxx     x                         xxx0",
        "xxxxx     x                         xxx0",
        "xxxxx                  x            xxx0",
        "xxxxxw                x  xx         xxx0",
        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx0"

    };


    //touchpad items
    private Viewport vp;
    private Stage stage;
    private Touchpad tpd;
    private TouchpadStyle ts;
    private Skin tskin;
    private Drawable touchBack;
    private Drawable touchFront;




    @Override
    public void create() {
        screenHeight = 480;
        screenWidth = 640;
        float ar = screenWidth / screenHeight;
        //batch = new SpriteBatch();7
        vp = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        //oc = new OrthographicCamera(640, 480);
        //oc.setToOrtho(false);

        stage = new Stage();

        oc = (OrthographicCamera) vp.getCamera();
        oc.setToOrtho(false, 640, 480);
        //stage.se



        p = new Player();



        tskin = new Skin();
        tskin.add("back", new Texture("touchBackground.png"));
        tskin.add("front", new Texture("touchKnob.png"));
        ts = new TouchpadStyle();
        touchBack = tskin.getDrawable("back");
        touchFront = tskin.getDrawable("front");
        ts.background = touchBack;
        ts.knob = touchFront;


        tpd = new Touchpad(8, ts);
        tpd.setBounds(100, 200, 150, 150);
        tpd.setPosition(80, 300);

        stage.addActor(tpd);
        Gdx.input.setInputProcessor(stage);


         ///newww
        //batch = (SpriteBatch) stage.getBatch();


        
        bf = new BitmapFont();
        bf.setColor(Color.WHITE);
        img = new Texture("hahaha.jpg");
        walkList = new Texture("walk.png");
        jumpList = new Texture("jump.png");
        idleList = new Texture("idle.png");
        punchList = new Texture("punch.png");
        box = new Texture("box.png");
        box2 = new Texture("box2.png");
        boxw = new Texture("boxw.png");
        ta = new TextureAtlas();
        woodBoxes = new ArrayList<WoodBox>();

        for (frame = 0; frame < 10; frame++) {
            ta.addRegion(Integer.toString(frame), walkList, width * frame, 0, width, height);
        }

        walkAnimation = new Animation(1 / 15f, ta.getRegions());
        p.animMap.put(Player.State.WALK, walkAnimation);

        ta = new TextureAtlas();

        for (int i = 0; i < 11; i++) {
            ta.addRegion(Integer.toString(i), jumpList, width * i, 0, width - 2, height);
        }
        jumpAnimation = new Animation(1 / 10f, ta.getRegions());
        p.animMap.put(Player.State.JUMP, jumpAnimation);

        ta = new TextureAtlas();
        for (int i = 0; i < 4; i++) {
            ta.addRegion(Integer.toString(i), idleList, width * i, 0, width, height);
        }
        idleAnimation = new Animation(1 / 5f, ta.getRegions());
        p.animMap.put(Player.State.IDLE, idleAnimation);

        ta = new TextureAtlas();
        for (int i = 0; i < 6; i++) {
            ta.addRegion(Integer.toString(i), punchList, (width + 38) * i, 0, width + 38, height);
        }
        punchAnimation = new Animation(1 / 12f, ta.getRegions());
        p.animMap.put(Player.State.PUNCH, punchAnimation);
        for (int i = 0; i < 10; i++) {
            woodBoxes.add(new WoodBox(200 + i * 50, 110, box, box2));
        }
    }

    @Override
    public void render() {
        time += Gdx.graphics.getDeltaTime();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        oc.update();
        
        if (Gdx.input.isKeyPressed(Keys.LEFT) || (tpd.getKnobPercentX() < 0)) {
            if (p.onGround && p.currentState != Player.State.PUNCH) {
                p.currentState = Player.State.WALK;
            }
            if (p.currentState == Player.State.JUMP || p.currentState == Player.State.WALK) {
                p.speedX = -2f;
            }

            if (rightDirection) {
                rightDirection = !rightDirection;
            }
        }
        if (Gdx.input.isKeyPressed(Keys.RIGHT) || (tpd.getKnobPercentX() > 0)) {
            if (p.onGround && p.currentState != Player.State.PUNCH) {
                p.currentState = Player.State.WALK;
            }
            if (p.currentState == Player.State.JUMP || p.currentState == Player.State.WALK) {
                p.speedX = 2f;
            }
            if (!rightDirection) {
                rightDirection = !rightDirection;
            }

        }
        if (Gdx.input.isKeyPressed(Keys.UP) || (tpd.getKnobPercentY() > 0.5)) {
            if (p.onGround) {
                p.currentState = Player.State.JUMP;
                p.speedY = 6f;
                p.onGround = false;
            }
        }
        if (Gdx.input.isKeyPressed(Keys.SPACE) || (tpd.getKnobPercentY() < -0.5)) {
                p.speedX = 0;
                p.currentState = Player.State.PUNCH;
                p.punchAnim = 10;
        }

        p.update(map);




        oc.position.set( p.positionX < screenWidth / 2 ? screenWidth / 2 :                                                          //left border
                        (50 * map[0].length() - p.positionX) > screenWidth / 2 ?  p.positionX : 50 * map[0].length() - screenWidth / 2, //right border
                p.positionY < screenHeight / 2 ? screenHeight / 2 : //bottom border
                        map.length * 50 - p.positionY > screenHeight / 2 ? p.positionY : map.length * 50 - screenHeight / 2, 0);



        //oc.position.set(p.positionX, p.positionY, 0);

        ///newww
        ;


        stage.getBatch().setProjectionMatrix(oc.combined);
        stage.getBatch().begin();


        tr = (TextureRegion) p.animMap.get(p.currentState).getKeyFrame(time, true);

        if(p.time420 && ((p.positionX / 50)  < 11) && (p.positionY / 50 < 6)){
            stage.getBatch().draw(img, 250, 50, 250, 250);
            p.time420 = false;
        }

        stage.getBatch().draw(tr, rightDirection ? p.positionX : p.positionX + tr.getRegionWidth(), p.positionY, rightDirection ? tr.getRegionWidth() : -tr.getRegionWidth(), height);



        for (int dy = (int)(((oc.position.y - screenHeight / 2)) / 50); dy < ((oc.position.y + (screenWidth / 2)) / 50) - 2 ; dy++) {
            String s = map[map.length - 1 - dy];
            for (int dx =  (int) ((oc.position.x - (screenWidth / 2)) / 50); dx < ((oc.position.x + (screenWidth / 2)) / 50); dx++) {
                char c = s.toCharArray()[dx];
               // bf.draw(stage.getBatch(), "" + dx, dx * 50, 50);
                if (c == 'x') {
                    stage.getBatch().draw(box, dx * 50, dy * 50);
                }
                if (c == 'b') {
                    stage.getBatch().draw(box2, dx * 50, dy * 50);
                }
                if (c == 'w') {
                    stage.getBatch().draw(boxw, dx * 50, dy * 50);
                }
            }
           // bf.draw(stage.getBatch(), "" + (map.length - dy), 50, dy * 50);
        }



    if(p.speedY < -6.5) {
        bf.draw(stage.getBatch(), "SUKA BLYAT'!111111", p.positionX, p.positionY + 100);
    }
       // bf.draw(stage.getBatch(), "1up " + (map.length - ((p.positionY) / 50 + 1)) +  " 1to" +  (map.length - ((p.positionY + 50) / 50) - 1) + " ", p.positionX, p.positionY + 130);





        /*
        if (p.positionX < Gdx.graphics.getWidth() / 2){
            oc.position.set(Gdx.graphics.getWidth() / 2, p.positionY < Gdx.graphics.getHeight() / 2 ? Gdx.graphics.getHeight() / 2 : p.positionY, 0);
        }else{
            oc.position.set((50 * 39 - p.positionX) > Gdx.graphics.getWidth() / 2 ? p.positionX : 50 * 39 - Gdx.graphics.getWidth() / 2, p.positionY < Gdx.graphics.getHeight() / 2 ? Gdx.graphics.getHeight() / 2 : p.positionY, 0);
        }
        */

        stage.getBatch().end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void dispose() {
        ta.dispose();
        stage.dispose();
    }


    @Override
    public void resize(int width, int height) {

        stage.getViewport().update(width, height, true);

    }


}
