/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Animation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Player {

    enum State {
        IDLE,
        WALK,
        JUMP,
        PUNCH
    }

    public Map<State, Animation> animMap;
    public int punchAnim;
    public int positionX;
    public int positionY;
    public float speedX;
    public float speedY;
    public boolean onGround;
    public State currentState;
    public boolean time420 = false;

    public Player() {
        positionX = 150;
        positionY = 100;
        onGround = false;
        currentState = State.IDLE;
        punchAnim = 0;
        animMap = new HashMap<State, Animation>();
    }

    void update(String[] map) {

        onGround = false;
        speedY -= 0.18f;
        positionX += speedX;


         for (int i = map.length - ((positionY + 50) / 50) - 1; i < map.length - (positionY / 50); i++) {
            for (int j = (positionX / 50); j < (positionX + 100) / 50; j++) {
                if (map[i].toCharArray()[j] == 'w' && currentState == State.PUNCH) {
                    time420 = true;
                   // System.out.println("Collides weed @" + j);
                }
                if (map[i].toCharArray()[j] == 'x') {
                   // System.out.println(i + " " + j);
                    if (speedX > 0) {
                        positionX = j * 50 - 52;
                    }
                    if (speedX < 0) {
                        positionX = j * 50 + 50;
                    }
                }
                //System.out.println(i + " " + j);
            }

        }

        if ((speedX == 0) && (currentState != State.JUMP) && (currentState != State.PUNCH)) {
            currentState = State.IDLE;
        }



        positionY += speedY;

        for (int i = map.length - ((positionY + 50) / 50) - 1; i < map.length - (positionY / 50); i++) {
            for (int j = (positionX  / 50); j < (positionX + 100) / 50; j++) {
                if (map[i].toCharArray()[j] == 'x') {
                    if (speedY > 0) {
                        positionY = (map.length - i) * 50 - 110;
                        speedY = 0;
                    }
                    if (speedY < 0) {
                        positionY = (map.length - i) * 50;
                        speedY = 0;
                        onGround = true;
                        //punchAnim = 0;
                        if(currentState == State.JUMP){
                            currentState = State.IDLE;
                            //System.out.println("84");
                        }
                        
                    }
                }
            }
        }

        speedX = 0;

        if(!onGround && currentState != State.PUNCH) {
            currentState = State.JUMP;
        }

        if (currentState == State.PUNCH && punchAnim > 0) {
            punchAnim--;

        } else if ((currentState == State.PUNCH) && (punchAnim == 0)) {
            currentState = State.IDLE;
        }
    }

}
