/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author Богдан
 */
public class WoodBox {

    Rectangle rect;
    boolean damaged = false;
    int x;
    int y;
    Texture box1;
    Texture box2;

    public WoodBox(int x, int y, Texture box1, Texture box2) {
        this.x = x;
        this.y = y;
        this.box1 = box1;
        this.box2 = box2;
        rect = new Rectangle(x, y, 50, 50);
    }
    
    public Texture getTexture(){
        return damaged ? box2 : box1;
    }
}
